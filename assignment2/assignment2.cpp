// assignment2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <vector>
#include <sstream>

using namespace std;

class InputTester
{

public:
	void testNumbers();

private:
	vector<vector<int>> getInput();
};


void InputTester::testNumbers()
{
	vector<vector<int>> cases;

	try
	{
		cases = getInput();
	}
	catch (runtime_error &err)
	{
		cerr << err.what() << endl;
		return;
	}

	bool found = false;

	for (auto numbersCase : cases)
	{
		for (int i = 0; i < numbersCase.size(); i++)
		{
			int number = numbersCase[i];
			int sumLeft = 0;
			int sumRight = 0;

			for (int j = 0; j < i; j++)
			{
				sumLeft += numbersCase[j];
			}

			for (int j = i + 1; j < numbersCase.size(); j++)
			{
				sumRight += numbersCase[j];
			}

			if (sumLeft == sumRight)
			{
				found = true;
				break;
			}
		}

		if (found)
			cout << "Yes" << endl;
		else
			cout << "No" << endl;
	}

}

vector<vector<int>> InputTester::getInput()
{
	int amountOfCases;
	int amountOfNumbers;
	vector<vector<int>> cases;
	string inputNumbers;
	int number;

	cout << "Cases: ";
	cin >> amountOfCases;

	for (int i = 0; i < amountOfCases; i++)
	{
		cout << "Amount: ";
		cin >> amountOfNumbers;

		cin.ignore();
		cout << "Sequence: ";
		getline(cin, inputNumbers);
		istringstream stream(inputNumbers);
		vector<int> numbers;

		while (stream >> number)
		{
			numbers.push_back(number);
		}

		if (numbers.size() != amountOfNumbers)
		{
			throw runtime_error("Invalid amount of numbers given.");
		}

		cases.push_back(numbers);
	}

	return cases;
}

int main()
{
	InputTester tester;

	tester.testNumbers();
    return 0;
}

